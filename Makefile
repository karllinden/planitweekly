#
#  This file is part of planitweekly.
#
#  Copyright (C) 2017 Karl Lindén <karl.j.linden@gmail.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

PREFIX   ?= /usr/local
BINDIR   := $(PREFIX)/bin
SHAREDIR := $(PREFIX)/share
ICONDIR  := $(SHAREDIR)/icons
DATADIR  := $(SHAREDIR)/planitweekly

SIZES = 16 22 24 32 36 48 64 72 96

all:
	$(NOOP)

install:
	install -D -t $(DESTDIR)$(BINDIR) planitweekly
	install -D -t $(DESTDIR)$(DATADIR) -m 0644 COPYING
	install -D \
		-t $(DESTDIR)$(SHAREDIR)/applications planitweekly.desktop
	for size in $(SIZES); do \
		install -D \
			-t $(DESTDIR)$(ICONDIR)/hicolor/$${size}x$${size}/apps/ \
			hicolor/$${size}x$${size}/apps/planitweekly.png; \
	done
	install -D -t $(DESTDIR)$(ICONDIR)/hicolor/scalable/apps/ \
		hicolor/scalable/apps/planitweekly.svg

uninstall:
	-rm -f $(BINDIR)/planitweekly
	-rm -f $(DATADIR)/COPYING
	-rm -df $(DATADIR)
	-rm -f $(SHAREDIR)/applications/planitweekly.desktop
	for size in $(SIZES); do \
		rm -f $(ICONDIR)/hicolor/$${size}x$${size}/apps/planitweekly.png; \
	done
	-rm -f $(ICONDIR)/hicolor/scalable/apps/planitweekly.svg

icon: hicolor/scalable/apps/planitweekly.svg
	for size in $(SIZES); do \
		mkdir -p hicolor/$${size}x$${size}/apps/; \
		inkscape -w $$size -h $$size \
			-e hicolor/$${size}x$${size}/apps/planitweekly.png \
			hicolor/scalable/apps/planitweekly.svg; \
	done
